package ru.remezov;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        removeTheDuplicates(createMap());
    }

    public static Map<String, Person> createMap()
    {
        Map<String, Person> book = new HashMap<>();
        Person person1 = new Person(29,"Петрова","жен");
        Person person2 = new Person(34, "Сидорова", "жен");
        Person person3 = new Person(34, "Тихонова", "жен");
        Person person4 = new Person(35, "Петров", "муж");
        book.put("Key1", person1);
        book.put("Key2", person1);
        book.put("Key3", person2);
        book.put("Key4", person3);
        book.put("Key5", person4);
        book.put("Key6", person4);
        return book;
    }

    public static void removeTheDuplicates(Map<String, Person> map) {

        //Создаем две мапы-клоны map для поиска дубликатов
        Map<String, Person> mapToSearch = new HashMap<>(map);
        Map<String, Person> mapToSearch0 = new HashMap<>(map);

        //Создаем пустую мапу для сохранения оригинала пары "ключ-значение"
        Map<String, Person> mapToCopy = new HashMap<>();
        for (Map.Entry<String, Person> personDuplicate: mapToSearch.entrySet()) {
            mapToSearch0.remove(personDuplicate.getKey());
            if (mapToSearch0.containsValue(personDuplicate.getValue())) {
                mapToCopy.put(personDuplicate.getKey(), personDuplicate.getValue());
                removeItemFromMapByValue(map, personDuplicate.getValue());
            }
        }
        map.putAll(mapToCopy);

        for (Map.Entry<String, Person> person: map.entrySet()) {
            System.out.println(person.getValue().toString());
        }
    }

    public static void removeItemFromMapByValue(Map<String, Person> map, Person value)
    {
        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }


}
